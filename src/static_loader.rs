#[macro_export]
macro_rules! dll_loader_static(
    (
        dll_ident: $dll_ident:ident,
        dll_paths: [ $($dll_path:expr),+ ],
        functions: [
            $(ident: $ident:ident,
            load_str: $load_str:expr,
            proto: $proto:ty),+
        ]
    ) => (

    pub static mut $dll_ident: $crate::Dll = $crate::DLL_STATIC_NULL_INITIALIZER;
    pub static mut loaded_library_path: Option<&'static str> = None; 
    $(
        pub static mut $ident: $crate::FuncPtr<$proto> = $crate::FuncPtr {func: unloaded::$ident as $proto, is_loaded: false };
    )+

    pub fn load_library_and_functions() -> Result<(), &'static str> {
        if let Err(err_str) = load_library() {
            return Err(err_str);
        }
        load_functions();
        Ok(())
    }
    pub fn unload_library_and_functions() -> Result<(), &'static str> {
        if let Err(err_str) = unload_library() {
            return Err(err_str);
        }
        unload_functions();
        Ok(())
    }
    fn load_library() -> Result<(), &'static str> {
        $(
            if let Ok(()) = unsafe { $dll_ident.load_library($dll_path) } {
                unsafe { loaded_library_path = Some($dll_path) };
                return Ok(());
            }
        )+
        Err("dll_loader: Failed to load any version of the library")
    }
    fn unload_library() -> Result<(), &'static str> {
        if let Err(err_str) = unsafe { $dll_ident.unload_library() } {
            return Err(err_str)
        }
        unsafe { loaded_library_path = None };
        Ok(())
    }
    fn load_functions() {
        unsafe {
            $(
                match $dll_ident.load_symbol($load_str) {
                    Ok(func) => {
                        $ident.set_func(::std::mem::transmute(func), true);
                    },
                    Err(_) => {
                        $ident.set_func(unloaded::$ident, false);
                    }
                }
            )+
        }
    }
    fn unload_functions() {
        unsafe {
            $(
                $ident.set_func(unloaded::$ident, false);
            )+
        }
    }
););