#![feature(std_misc, collections)]

//TODO: Try overriding function call for FuncPtr to automatically call func member?
// Need some sort of more elegant solution then <FUNCTION>.func(), but will do for now.

//TODO: Similar to gl-rs' changes maybe make this generate a file with the code as a build step.

extern crate kernel32_sys;
extern crate win32_types;

use std::{ptr, mem, ffi};
use win32_types::{BOOL, HMODULE};
use kernel32_sys::{GetProcAddress, LoadLibraryW, FreeLibrary};

mod static_loader;



pub const DLL_STATIC_NULL_INITIALIZER: Dll = Dll { handle: 0 as HMODULE };

pub type GenericFunction = extern fn() -> isize;

//TODO Should they be assertions, do I want it to fail? Or just return sucess?
#[allow(missing_copy_implementations)]
pub struct Dll {
    handle: HMODULE,
}
impl Dll {
    pub fn new() -> Dll {
        Dll {
            handle: 0 as HMODULE,
        }
    }
    pub fn load_library(&mut self, dll_path: &str) -> Result<(), &'static str> {
        debug_assert!(self.handle == ptr::null_mut());
        self.handle = unsafe { LoadLibraryW(utf8_to_null_term_utf16(dll_path).as_ptr()) };
        if self.handle.is_null() {
            return Err("dll_loader: Failed to load library");
        }
        Ok(())
    }
    pub fn unload_library(&mut self) -> Result<(), &'static str> {
        debug_assert!(self.handle != ptr::null_mut());
        if unsafe { FreeLibrary(self.handle) } == BOOL::FALSE {
            return Err("dll_loader: Failed to unload library");
        }
        self.handle = ptr::null_mut();
        Ok(())
    }
    pub unsafe fn load_symbol(&self, symbol_name: &str) -> Result<GenericFunction, &'static str> {
        debug_assert!(self.handle != ptr::null_mut());
        let symbol_ptr = GetProcAddress(self.handle, ffi::CString::from_slice(symbol_name.as_bytes()).as_ptr());
        if (symbol_ptr as *mut u8) == ptr::null_mut() { //u8 is just a random type
            return Err("dll_loader: Failed to load symbol");
        }
        Ok(mem::transmute(symbol_ptr))
    }
    pub fn is_loaded(&self) -> bool {
        self.handle != ptr::null_mut()
    }
}

pub struct FuncPtr<F> {
    pub func: F,
    pub is_loaded: bool,
}
impl<F> FuncPtr<F> {
    #[inline]
    #[allow(dead_code)]
    pub fn is_loaded(&self) -> bool {
        self.is_loaded
    }
    #[inline]
    #[allow(dead_code)]
    pub fn func(&self) -> &F {
        &self.func
    }
    // primarily just to set it to the unloaded::self. Setting to func from another dll would be very unsafe
    #[inline]
    #[allow(dead_code)]
    pub unsafe fn set_func(&mut self, new_func: F, is_loaded: bool) {
        self.func = new_func;
        self.is_loaded = is_loaded;
    }
}

fn utf8_to_null_term_utf16(string: &str) -> Vec<u16> {
    string.utf16_units().chain(Some(0).into_iter()).collect()
}
