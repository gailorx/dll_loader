#[macro_export]
macro_rules! dll_loader_struct(
    (struct_name: $struct_name:ident, dll_names: [ $($dll_name:expr),+ ], functions: [ $(identifier: $function_name:ident, string: $function_string:expr, prototype: $function_ptr_type:ty),+ ]) => (
        pub struct FuncPtr<F> {
            pub func: F,
            pub is_loaded: bool,
        }
        mod __dll_loader_imports {
            extern crate "win32" as dll_import_win32;
            pub use self::dll_import_win32::kernel::{LoadLibraryW, FreeLibrary, GetProcAddress, FARPROC};
            pub use self::dll_import_win32::types::HMODULE;
            pub use std::ptr::null_mut;
            pub use std::mem::{transmute, zeroed};
        }
        struct $struct_name {
            dll_handle: __dll_loader_imports::HMODULE,
            dll_name: Option<*const u16>,
            is_dll_loaded: bool,
            $(
                pub $function_name: self::FuncPtr<$function_ptr_type>
            ),+
        }
        impl $struct_name {
            pub fn new() -> $struct_name {
                $struct_name {
                    dll_handle: __dll_loader_imports::null_mut(),
                    dll_name: None,
                    is_dll_loaded: false,
                    $(
                        $function_name: self::FuncPtr {func: unloaded::$function_name as $function_ptr_type, is_loaded: false }
                    ),+
                }
            }
            pub fn load_dll_and_functions(&mut self) -> Result<(), &'static str> {
                let result = self.load_dll();
                if result.is_ok() {
                    self.load_functions_pointers();
                    Ok(())
                }
                else {
                    result
                }
            }
            pub fn load_dll(&mut self) -> Result<(), &'static str> {
                if self.dll_handle.is_null() {
                    $(
                        self.dll_handle = unsafe { __dll_loader_imports::LoadLibraryW($dll_name) };
                        if self.dll_handle.is_not_null() {
                            self.is_dll_loaded = true;
                            self.dll_name = Some($dll_name);
                            return Ok(());
                        }
                    )+
                    return Err("dll_loader: Failed to load library");
                }
                Ok(())
            }
            pub fn unload_dll(&mut self) -> Result<(), &'static str> {
                if self.is_dll_loaded {
                    if unsafe { __dll_loader_imports::FreeLibrary(self.dll_handle) } == 0 {
                        return Err("dll_loader: Failed to unload library");
                    }
                    self.dll_name = None;
                	self.is_dll_loaded = false;
                	self.unload_functions_pointers();
                }
                Ok(())
            }
            pub fn load_functions_pointers(&mut self) {
                let mut temp_ptr: __dll_loader_imports::FARPROC = unsafe { __dll_loader_imports::zeroed() };
                $(
                    temp_ptr = unsafe { __dll_loader_imports::GetProcAddress(self.dll_handle, $function_string.to_c_str().as_ptr()) };
                    self.$function_name.func = if (temp_ptr as *mut u8) != __dll_loader_imports::null_mut() { //u8 is just a random type i tried
                        self.$function_name.is_loaded = true;
                        unsafe { __dll_loader_imports::transmute::<__dll_loader_imports::FARPROC, $function_ptr_type>(temp_ptr) }
                    }
                    else {
                        unloaded::$function_name
                    };
                )+
            }
            pub fn unload_functions_pointers(&mut self) {
                $(
                    self.$function_name.func = unloaded::$function_name;
                    self.$function_name.is_loaded = false;
                )+
            }
        }
    );
);